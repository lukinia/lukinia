<?php

namespace App\Controller;

use App\Service\DaneUzytkownikaService;
use App\Service\LoginService;
use App\Service\ZgloszeniaService;
use phpDocumentor\Reflection\Types\Integer;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Route as ROUTING;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class StronaZgloszeniaController extends AbstractController
{

    private $daneUzytkownikaService;
    private $loginService;
    private $zgloszeniaService;
    private $logger;

    public function  __construct(DaneUzytkownikaService $daneUzytkownikaService,ZgloszeniaService $zgloszeniaService,
                                 LoginService $loginService, LoggerInterface $logger) {

        $this->daneUzytkownikaService = $daneUzytkownikaService;
        $this->loginService = $loginService;
        $this->zgloszeniaService = $zgloszeniaService;
        $this->logger = $logger;
    }

    /**
     * @Route("/stronaZgloszenia", methods={"GET"})
     */
    public function stronaZgloszeniaGet() {

        $Id = $this->loginService->dostepUzytkownikaService();

        if ( $Id==false ) {

            return $this->redirect(parent::getParameter('baseUrl')."logowanie");
        }

        $daneUzytkownikaArr = $this->daneUzytkownikaService->pobierzDaneUzytkownikaService( $Id );

        $slownikKlienci = $this->zgloszeniaService->slownikKlienciService();
        $slownikKategoria = $this->zgloszeniaService->slownikKategoriaService();
        $slownikPriorytet = $this->zgloszeniaService->slownikPriorytetService();
        $slownikStatus = $this->zgloszeniaService->slownikStatusService();
        $listaWykonawcow = $this->zgloszeniaService->listaWykonawcowService();
        $zgloszeniaArr = $this->zgloszeniaService->zgloszeniaService();
        $zgloszeniaHistoriaArr = $this->zgloszeniaService->zgloszeniaHistService();

        return $this->render('stronaZgloszenia.html.twig',
            array( 'daneUzytkownikaArr'=>$daneUzytkownikaArr,'slownikKlienci'=>$slownikKlienci,
                'slownikKategoria'=>$slownikKategoria,'slownikStatus'=>$slownikStatus,'wykonawcaArr'=>$listaWykonawcow,
                'slownikPriorytet'=>$slownikPriorytet,'zgloszeniaArr'=>$zgloszeniaArr,'zgloszeniaHistoriaArr'=>$zgloszeniaHistoriaArr) );
    }

    /**
     * @Route("/stronaZgloszenia", methods={"POST"})
     */
    public function stronaZgloszeniaPost() {

        return $this->redirect(parent::getParameter('baseUrl')."");
    }


    /**
     *
     * dynamiczne generowanie słownika adresów
     *
     * @Route("/dynamicznyAdres/ajax", methods={"POST"})
     */
    public function klientDynamicznySlownik(Request $request) {

        $adresSlownikArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $adresSlownik = $this->zgloszeniaService->adresSlownikService($adresSlownikArr);

        $adresyDynamiczneArr = ['adresSlownik'=>$adresSlownik];

        return new JsonResponse($adresyDynamiczneArr);
    }

    /**
     *
     * zapisywanie zgłoszenia. Funkcja zwraca też aktualny stan tabeli zgłoszeń.
     *
     * @Route("/zapiszZgloszenie/ajax", methods={"POST"})
     */
    public function zapiszZgloszenie(Request $request) {

        $zgloszeniaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $zgloszeniaArr =  $this->zgloszeniaService->zapiszZgloszenieService($zgloszeniaArr);

        $zgloszeniaTab = ['zgloszeniaArr'=>$zgloszeniaArr];

        return new JsonResponse($zgloszeniaTab);
    }

    /**
     *
     *
     *
     * @Route("/listaZgloszen/ajax", methods={"POST"})
     */
    public function listaZgloszen(Request $request) {

        $zgloszeniaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $listaZgloszen = $this->zgloszeniaService->listaZgloszenService($zgloszeniaArr);

        $wykonawca = $this->zgloszeniaService->wykonawcaService($zgloszeniaArr);

        $zgloszeniaTab = ['zgloszeniaArr'=>$listaZgloszen,'wykonawcaArr'=>$wykonawca];

        return new JsonResponse($zgloszeniaTab);
    }

    /**
     *
     * edytowanie zgłoszenia. Funkcja zwraca też aktualny stan tabeli zgłoszeń.
     *
     * @Route("/edytujZgloszenie/ajax", methods={"POST"})
     */
    public function edytujZgloszenie(Request $request) {

        $zgloszeniaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $zgloszenia =  $this->zgloszeniaService->edytujZgloszenieService($zgloszeniaArr);

        $zgloszeniaTab = ['zgloszeniaArr'=>$zgloszenia];

        return new JsonResponse($zgloszeniaTab);
    }

    /**
     *
     * filtrowanie zgloszen
     *
     * @Route("/filtrujZgloszenia/ajax", methods={"POST"})
     */
    public function filtrujZgloszenia(Request $request) {

        $zgloszeniaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $filtrujZgloszenia =  $this->zgloszeniaService->filtrujZgloszeniaService($zgloszeniaArr);

        $zgloszeniaTab = ['filtrujZgloszeniaArr'=>$filtrujZgloszenia];

        return new JsonResponse($zgloszeniaTab);
    }

    /**
     *
     * dodawanie załączników
     *
     * @Route("/dodajZalacznik/ajax", methods={"POST"})
     */
    public function zapiszPlik(Request $request) {

        if (!file_exists('C:\zzz')) {
            mkdir('C:\zzz');
        }

        $filename = $_FILES['file']['size'].time().'_'.$_FILES['file']['name'];
        $sciezka = 'C:\zzz/'.$filename;
        move_uploaded_file($_FILES['file']['tmp_name'], $sciezka);

        $IdZgloszeniaString = $_POST['IdZgloszenia'];
        $IdZgloszeniaInt = (int)$IdZgloszeniaString;

        $this->zgloszeniaService->zapiszPlikService($sciezka,$IdZgloszeniaInt);

        return new JsonResponse($_FILES['file']['name']);
    }

    /**
     *
     * po zmianie w tabeli historycznej selecta status wykonuje sie ta funkcja
     *
     * @Route("/historiaZgloszenEdycja/ajax", methods={"POST"})
     */
    public function historiaZgloszenEdycja(Request $request) {

        $zgloszeniaHistoriaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $listaZgloszenHistoria = $this->zgloszeniaService->historiaZgloszenEdycjaService($zgloszeniaHistoriaArr);

        $zgloszeniaHistoriaTab = ['zgloszeniaHistoriaArr'=>$listaZgloszenHistoria];

        return new JsonResponse($zgloszeniaHistoriaTab);
    }

    /**
     *
     * po kliknieciu przycisku pokaz historie wykonuje sie ta funkcja
     *
     * @Route("/historiaZgloszen/ajax", methods={"POST"})
     */
    public function historiaZgloszen(Request $request) {

        $zgloszeniaHistoriaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $listaZgloszenHistoria = $this->zgloszeniaService->historiaZgloszenService();

        $zgloszeniaHistoriaTab = ['zgloszeniaHistoriaArr'=>$listaZgloszenHistoria];

        return new JsonResponse($zgloszeniaHistoriaTab);
    }

    /**
     *
     * po kliknieciu przycisku pokaz zgloszenia wykonuje sie ta funkcja
     *
     * @Route("/pokazZgloszenia/ajax", methods={"POST"})
     */
    public function pokazZgloszenia(Request $request) {

        $zgloszeniaHistoriaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $listaZgloszen = $this->zgloszeniaService->pokazZgloszenService();

        $zgloszeniaTab = ['zgloszeniaArr'=>$listaZgloszen];

        return new JsonResponse($zgloszeniaTab);
    }

    /**
     *
     * filtrowanie zgloszen historycznych
     *
     * @Route("/filtrujZgloszeniaHistoria/ajax", methods={"POST"})
     */
    public function historiaZgloszenFiltr(Request $request) {

        $zgloszeniaFiltrHistoriaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $listaZgloszenHistoriaFiltr = $this->zgloszeniaService->historiaZgloszenFiltrService($zgloszeniaFiltrHistoriaArr);

        $zgloszeniaFiltrHistoriaTab = ['zgloszeniaFiltrHistoriaArr'=>$listaZgloszenHistoriaFiltr];

        return new JsonResponse($zgloszeniaFiltrHistoriaTab);
    }

    /**
     *
     * wyswietlanie komentarzy
     *
     * @Route("/wyswietlKomentarzeZmiany/ajax", methods={"POST"})
     */
    public function wyswietlKomentarzeZmiany(Request $request) {

        $wyswietlKomentarzeZmianyArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $wyswietlKomentarze = $this->zgloszeniaService->wyswietlKomentarzeService($wyswietlKomentarzeZmianyArr);
        $wyswietlZmiany = $this->zgloszeniaService->wyswietlZmianyService($wyswietlKomentarzeZmianyArr);

        $wyswietlKomentarzeTab = ['wyswietlKomentarzeTab'=>$wyswietlKomentarze,'wyswietlZmianyTab'=>$wyswietlZmiany];

        return new JsonResponse($wyswietlKomentarzeTab);
    }


    /**
     *
     * dodawanie komentarzy
     *
     * @Route("/dodawanieKomentarza/ajax", methods={"POST"})
     */
    public function dodawanieKomentarza(Request $request) {

        $dodawanieKomentarzaArr = $request->request->get('tab');

        $this->logger->info('!!!!!!!!!!!!!!!!!!!!   kontroler');

        $dodawanieKomentarza = $this->zgloszeniaService->dodawanieKomentarzaService($dodawanieKomentarzaArr);

        $dodawanieKomentarzaTab = ['dodawanieKomentarzaTab'=>$dodawanieKomentarza];

        return new JsonResponse($dodawanieKomentarzaTab);
    }

}